#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

int eh_no_vermelho(ArvoreRB * no){
  if(!no) return BLACK;
  return(no->cor == RED);
}

ArvoreRB* cria_nodo(int info, int cor){
  ArvoreRB *a = (ArvoreRB*) malloc(sizeof(ArvoreRB));
  a->info = info;
  a->esq = a->dir = NULL;
  a->cor = RED;
  return a;
}

ArvoreRB* roda_esq(ArvoreRB* esq){
  ArvoreRB *dir = esq->dir;
  esq->dir = dir->esq;
  dir->esq = esq;
  dir->cor = esq->cor;
  esq->cor = RED;
  return (dir);
}

ArvoreRB* roda_dir(ArvoreRB* dir){
  ArvoreRB *esq = dir->esq;
  dir->esq = esq->dir;
  esq->dir = dir;
  esq->cor = dir->cor;
  dir->cor = RED;
  return (esq);
}

void inverte_cores(ArvoreRB *a){
  a->cor = !(a->cor);
  a->esq->cor = !(a->esq->cor);
  a->dir->cor = !(a->dir->cor);
}

ArvoreRB* inserir(ArvoreRB *a, int info){
  if (a == NULL)
    return cria_nodo(info, 0);   
 
  if(info < a->info)
    a->esq = inserir(a->esq, info);
  else if(info > a->info)
    a->dir = inserir(a->dir, info);
  else   
    return a;

  if(eh_no_vermelho(a->dir) && !eh_no_vermelho(a->esq)){
    a = roda_esq(a);
  }

  if(eh_no_vermelho(a->esq) && eh_no_vermelho(a->esq->esq)){  
    a = roda_dir(a);
  }

  if(eh_no_vermelho(a->esq) && eh_no_vermelho(a->dir)){
    inverte_cores(a);
  }
 
  return a;
}

int buscar (ArvoreRB *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}

void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

void print(ArvoreRB *a, int space) {
  if (a == NULL)
    return;

  space += 5;

  print(a->dir, space);

  printf("\n");
  for (int i = 5; i < space; i++)
      printf(" ");
  if(a->cor) printf("|");
  printf("%d\n", a->info);

  print(a->esq, space);
}

int main(){
  ArvoreRB * a = NULL;

  a = inserir(a,20);
  a->cor = BLACK;
  a = inserir(a,30);
  a->cor = BLACK;
  a = inserir(a,35);
  a->cor = BLACK;
  a = inserir(a,90);
  a->cor = BLACK;
  a = inserir(a,95);
  a->cor = BLACK;
  a = inserir(a,37);
  a->cor = BLACK;
  a = inserir(a,40);
  a->cor = BLACK;
  a = inserir(a,45);
  a->cor = BLACK;
  a = inserir(a,50);
  a->cor = BLACK;
  a = inserir(a,10);
  a->cor = BLACK;

  printf("\n");
  print(a,0);
  printf("\n");
}
